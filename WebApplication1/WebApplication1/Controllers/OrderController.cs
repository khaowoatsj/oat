﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using WebApplication1.Model;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebApplication1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("MyPolicy")]
    public class OrderController : Controller
    {
        
        private readonly IHostingEnvironment _hostingEnvironment;

        public OrderController(IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
        }

        // GET: api/Order
        [HttpGet]
        public JsonResult Get()
        {
            string contentRootPath = _hostingEnvironment.ContentRootPath;
            var dataJSON = System.IO.File.ReadAllText(contentRootPath + "/orders.json");

            List<Product> json = JsonConvert.DeserializeObject<List<Product>>(dataJSON);
            


            return Json(json);
        }

       
        
        // POST: api/Order
        [HttpPost]
        public JsonResult Post([FromBody]Product product)
        {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            var stringChars = new char[8];
            var random = new Random();

            for (int i = 0; i < stringChars.Length; i++)
            {
                stringChars[i] = chars[random.Next(chars.Length)];
            }

            var finalString = new String(stringChars);
            string Timestamp = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds().ToString() + finalString;
            string contentRootPath = _hostingEnvironment.ContentRootPath;
            var dataJSON = System.IO.File.ReadAllText(contentRootPath + "/orders.json");
            List<Product> products = JsonConvert.DeserializeObject<List<Product>>(dataJSON);
            products.Add(new Product {
                idpd = Timestamp,
                pdname = product.pdname,
                pdprice = product.pdprice,
                pdcount = product.pdcount,
                pdcity = product.pdcity,
                pdtype = product.pdtype,
                pddetail = product.pddetail,
                pdimg = product.pdimg
            });
            System.IO.File.WriteAllText("orders.json", JsonConvert.SerializeObject(products));
            return Json(products);
        }

        // PUT: api/Order/5
        [HttpPut("{pdimg}")]
        public JsonResult Put(string pdimg)
        {
            string contentRootPath = _hostingEnvironment.ContentRootPath;
            var dataJSON = System.IO.File.ReadAllText(contentRootPath + "/orders.json");
            List<Product> json = JsonConvert.DeserializeObject<List<Product>>(dataJSON);

            return Json(json);

        }
        
        // DELETE: api/ApiWithActions/5
        [HttpDelete("{pdid}")]
        public void Delete(string pdid)
        {
            //http://localhost:49855/api/product/ชื่อที่จะลบ
            string contentRootPath = _hostingEnvironment.ContentRootPath;
            var dataJSON = System.IO.File.ReadAllText(contentRootPath + "/orders.json");
            List<Product> products = JsonConvert.DeserializeObject<List<Product>>(dataJSON);
            for (int i = 0; i < products.Count; i++)
            {
                if (products[i].idpd.Equals(pdid))
                {
                    products.RemoveAt(i);
                }
            }
            System.IO.File.WriteAllText("orders.json", JsonConvert.SerializeObject(products));

        }
        
        
    }
}
