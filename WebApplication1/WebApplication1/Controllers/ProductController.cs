﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using WebApplication1.Model;


namespace WebApplication1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("MyPolicy")]
    public class ProductController : Controller
    {
        private readonly IHostingEnvironment _hostingEnvironment;

        public ProductController(IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
        }

        // GET: api/Product
        [HttpGet]
        public JsonResult Get()
        {
            string contentRootPath = _hostingEnvironment.ContentRootPath;
            var dataJSON = System.IO.File.ReadAllText(contentRootPath + "/products.json");

            List<Product> json = JsonConvert.DeserializeObject<List<Product>>(dataJSON);
            return Json(json);
        }

        // GET: api/Product/5
        [HttpGet("{id}", Name = "Get")]
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Product
        [HttpPost]
        public JsonResult Post([FromBody]Product product)
        {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            var stringChars = new char[8];
            var random = new Random();

            for (int i = 0; i<stringChars.Length; i++)
            {
                stringChars[i] = chars[random.Next(chars.Length)];
            }

            var finalString = new String(stringChars);
            string Timestamp = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds().ToString() + finalString;
            string contentRootPath = _hostingEnvironment.ContentRootPath;
            var dataJSON = System.IO.File.ReadAllText(contentRootPath + "/products.json");
            List<Product> products = JsonConvert.DeserializeObject<List<Product>>(dataJSON);
            products.Add(new Product { idpd = Timestamp, pdname = product.pdname, pdprice = product.pdprice });
            System.IO.File.WriteAllText("products.json", JsonConvert.SerializeObject(products));
            return Json(products);
        }

        // PUT: api/Product/5
        [HttpPut]
        public void Put([FromBody] Product value)
        {
            string contentRootPath = _hostingEnvironment.ContentRootPath;
            var dataJSON = System.IO.File.ReadAllText(contentRootPath + "/products.json");
            List<Product> products = JsonConvert.DeserializeObject<List<Product>>(dataJSON);
            for (int i =0; i < products.Count; i++)
            {
                if (products[i].pdname.Equals(value.pdname))
                {
                    products[i].pdname = value.pdname;
                    products[i].pdprice = value.pdprice;
                }
            }
            System.IO.File.WriteAllText("products.json", JsonConvert.SerializeObject(products));
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{pdname}")]
        public void Delete(string pdname)
        {
            //http://localhost:49855/api/product/ชื่อที่จะลบ
            string contentRootPath = _hostingEnvironment.ContentRootPath;
            var dataJSON = System.IO.File.ReadAllText(contentRootPath + "/products.json");
            List<Product> products = JsonConvert.DeserializeObject<List<Product>>(dataJSON);
            for (int i = 0; i < products.Count; i++)
            {
                if (products[i].pdname.Equals(pdname))
                {
                    products.RemoveAt(i);
                }
            }
            System.IO.File.WriteAllText("products.json", JsonConvert.SerializeObject(products));
            
        }
    }
}
