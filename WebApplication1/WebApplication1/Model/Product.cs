using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Model
{
    public class Product
    {
        public string idpd { get; set; }
        public string pdname { get; set; }
        public double pdprice { get; set; }
        public double pdcount { get; set; }
        public string pdcity { get; set; }
        public string pdtype { get; set; }
        public string pddetail { get; set; }

        public string pdimg { get; set; }

        //public static implicit operator List<object>(Product v)
        //{
        //    throw new NotImplementedException();
        //}
    }
}